TopBraid EDG Role (WIP)
=======================

Installs TopBraid EDG

# Prerequisites
- A license and executable (edg.war) from Topquadrant.
- Ansible
- NFS

# Installation

## Create a Host File and Setup SSH
Use the host.example for an example of AWS EC2 configuration.

# Usage

## Provision Data Coordinator
`ansible-playbook rdfdelta.yml -i ./hosts.example`

## Provision TopBraid EDG
`ansible-playbook topbraid.yml -i ./hosts.example`

## Provision Proxy/Load Balancer
`ansible-playbook proxy.yml -i ./hosts.example`

# Reconfigure TopBraid EDG
`ansible-playbook topbraid.yml -i ./hosts.example --start-at-task="configure EDG"`

# Maintanance

## Install a new EDG release

EDG has different files wich could be released individually. 
Any change in one of those files will result in a new platform release.

### EDG Web Application
1. Obtain the appropiate files from TopQuadrant
2. Change the release version in the inventory (release_version=<version>)
3. Mount the NFS using: `ansible-playbook playbooks/create-and-mount-release-folder.yml -i ./hosts.example`
4. `sudo cp /mnt/nfs-mount/<previous-version>/topbraid/dropins/* /mnt/nfs-mount/<version>/topbraid/dropins/`
5. `sudo cp /mnt/nfs-mount/<previous-version>/topbraid/* /mnt/nfs-mount/<version>/topbraid/`
6. `cp edg.war /mnt/nfs-mount/<version>/topbraid/`

### EDG Data Coordinator
1. Repeat step 1-5 from EDG web application
2. `cp rdf-delta-server.jar /mnt/nfs-mount/<version>/topbraid/`

### EDG Webservices
1. Repeat step 1-5 from EDG web application
2. `cp <service-name>.jar /mnt/nfs-mount/<version>/topbraid/dropins`

# Troubleshooting

## 1. Cannot Add New Data
Start by looking at the EDG log file `ansible-playbook playbooks/edg-show-log.yml -i ./hosts.example`

### Possible Causes
Delta Server is not started (`org.apache.http.conn.HttpHostConnectException: Connect to 3.122.251.6:1066 [/3.122.251.6] failed: Connection refused ..."`)

## 2. Delta Server is not started
See the Delta Server log: `ansible-playbook playbooks/rdfdelta-last-patches.yml -i ./hosts.example`

### Possible Causes
NFS is not mounted (`No such directory: /mnt/nfs-mount`)
